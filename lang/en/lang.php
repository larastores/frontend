<?php

return [
    'list' => [
        'missing_columns' => 'List used in :class has no list columns defined.',
    ],
    'partial' => [
        'not_found_name' => "The partial ':name' is not found."
    ],
];
